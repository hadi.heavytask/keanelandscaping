import 'package:flutter/material.dart';

class ServiceModel {
  final String name;
  final String imageUrl;

  ServiceModel({required this.name, required this.imageUrl});
}

final List<ServiceModel> serviceList = [
  ServiceModel(
      name: "Landscape Installation", imageUrl: "images/arbors_fencing.png"),
  ServiceModel(
      name: "Lawn Service & Maintenance", imageUrl: "images/lawn_service.png"),
  ServiceModel(
      name: "Tree Removal & Trimming", imageUrl: "images/tree_removal.png"),
  ServiceModel(
      name: "Landscape Lighting", imageUrl: "images/landscape_lighting.png"),
  ServiceModel(
      name: "Irrigation & Sprinkler Repair",
      imageUrl: "images/irrigation_sprinkler.png"),
  ServiceModel(
      name: "Custom Stonework, Concrete & Masonry",
      imageUrl: "images/custom_stonework.png"),
  ServiceModel(
      name: "Arbors, Fencing & Woodwork",
      imageUrl: "images/arbors_fencing.png"),
  ServiceModel(name: "Pool Services", imageUrl: "images/pool_services.png"),
];

/*
"",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
 */
